//
//  City.h
//  Your Weather
//
//  Created by Vladimir on 15.05.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface City : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSNumber * lon;
@property (nonatomic, retain) NSNumber * lan;

@end
