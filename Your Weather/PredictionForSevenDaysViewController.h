//
//  PredictionForSevenDaysViewController.h
//  Your Weather
//
//  Created by Vladimir on 15.05.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PredictionForSevenDaysViewController : UIViewController

@property (nonatomic) NSString *cityName;
@property (nonatomic) NSString *imageKey;

@end
