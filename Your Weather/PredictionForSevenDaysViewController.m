//
//  PredictionForSevenDaysViewController.m
//  Your Weather
//
//  Created by Vladimir on 15.05.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import "PredictionForSevenDaysViewController.h"
#import <OpenWeatherMapAPI/OWMWeatherAPI.h>
#import "Constants.h"
#import "Prediction.h"
#import "PredictionCell.h"

@interface PredictionForSevenDaysViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic) OWMWeatherAPI *weatherAPI;
@property (nonatomic) int days;
@property (nonatomic) NSMutableArray *predictionArray;

@property (weak, nonatomic) IBOutlet UITableView *predictionTableView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;


@end

@implementation PredictionForSevenDaysViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.predictionArray = [NSMutableArray new];
    self.days = DAYS;
    
    self.weatherAPI = [[OWMWeatherAPI alloc] initWithAPIKey:@"ce8f6ec4317c5a92bdbe80a7f07d4c08"];
    [self.weatherAPI setTemperatureFormat:kOWMTempCelcius];
    
    [self.weatherAPI dailyForecastWeatherByCityName:self.cityName withCount:self.days andCallback:^(NSError *error, NSDictionary *result) {
        if (error) {
            [self.activityIndicator stopAnimating];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"No internet connection" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
            [self.navigationController popViewControllerAnimated:YES];
            return;
        }
        
        NSArray *resultArray = result[@"list"];
        for (NSDictionary *resultForOneDay in resultArray) {
            NSArray *weatherArray = resultForOneDay[@"weather"];
            NSDictionary *weatherDictionary = weatherArray[0];
            
            Prediction *prediction = [Prediction new];
            prediction.main = weatherDictionary[@"main"];
            prediction.weatherDescription = weatherDictionary[@"description"];
            prediction.temp = resultForOneDay[@"temp"][@"day"];
            prediction.windSpeed = resultForOneDay[@"speed"];
            prediction.cityName = self.cityName;
            prediction.date = resultForOneDay[@"dt"];
            [self.predictionArray addObject:prediction];
        }
        
        [self.activityIndicator stopAnimating];
        self.imageView.image = [UIImage imageNamed:self.imageKey];
        self.imageView.contentMode = UIViewContentModeScaleAspectFill;
        
        self.predictionTableView.hidden = NO;
        [self.predictionTableView reloadData];
    }];
}

#pragma mark Table View Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.predictionTableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)deselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.predictionTableView deselectRowAtIndexPath:indexPath animated:YES];
}

//- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return YES;
//}

#pragma mark Table View Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.predictionArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifierUserCell = @"PredictionCell";
    PredictionCell *cell = [tableView dequeueReusableCellWithIdentifier:identifierUserCell];
    Prediction *prediction = (Prediction *)self.predictionArray[indexPath.row];
    
    NSDateFormatter *format = [NSDateFormatter new];
    format.dateFormat = @"dd-MM-yyyy";
    cell.dateLabel.text = [format stringFromDate:prediction.date];
    cell.descriptionLabel.text = prediction.weatherDescription;
    cell.tempLabel.text = [NSString stringWithFormat:@"%ld\u00B0C", (long)[prediction.temp integerValue]];
    cell.windSpeedLabel.text = [NSString stringWithFormat:@"%ld m/s", (long)[prediction.windSpeed integerValue]];
    
    return cell;
    
}

@end
