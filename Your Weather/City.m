//
//  City.m
//  Your Weather
//
//  Created by Vladimir on 15.05.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import "City.h"


@implementation City

@dynamic name;
@dynamic id;
@dynamic lon;
@dynamic lan;

@end
