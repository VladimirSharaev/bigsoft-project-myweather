//
//  CitiesCell.h
//  Your Weather
//
//  Created by Vladimir on 15.05.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CitiesCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameCityLabel;

@end
