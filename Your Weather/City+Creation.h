//
//  City+Creation.h
//  Your Weather
//
//  Created by Vladimir on 15.05.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import "City.h"

@interface City (Creation)

+ (City *)cityWithName:(NSString *)name id:(NSNumber *)ID lon:(NSNumber *)lon lan:(NSNumber *)lan;

@end
