//
//  PredictionForOneDayViewController.m
//  Your Weather
//
//  Created by Vladimir on 15.05.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import "PredictionForOneDayViewController.h"
#import <OpenWeatherMapAPI/OWMWeatherAPI.h>
#import "City+Creation.h"
#import "Prediction.h"
#import "AppDelegate.h"
#import "PredictionForSevenDaysViewController.h"

@interface PredictionForOneDayViewController ()

@property (nonatomic) OWMWeatherAPI *weatherAPI;
@property (nonatomic) City *currentCity;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (nonatomic) Prediction *prediction;

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *cityNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *tempLabel;
@property (weak, nonatomic) IBOutlet UILabel *windSpeedLabel;
@property (weak, nonatomic) IBOutlet UILabel *temperatureLabel;
@property (weak, nonatomic) IBOutlet UILabel *windLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIButton *predictionForSevenDaysButton;


- (void)changeLabelTextColor:(UIColor *)color;
- (void)hiddenAllTextLabel:(BOOL)hidden;
- (void)changeImage:(NSString *)mainString;
- (void)addInformationOnView;

- (IBAction)predictionForSevenDaysButton:(id)sender;

@end

@implementation PredictionForOneDayViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    self.managedObjectContext = appDelegate.managedObjectContext;
    
    self.weatherAPI = [[OWMWeatherAPI alloc] initWithAPIKey:@"ce8f6ec4317c5a92bdbe80a7f07d4c08"];
    [self.weatherAPI setTemperatureFormat:kOWMTempCelcius];
    
    [self.weatherAPI currentWeatherByCityName:self.cityName withCallback:^(NSError *error, NSDictionary *result) {
        if (error) {
            [self.activityIndicator stopAnimating];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"No internet connection" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
            [self.navigationController popViewControllerAnimated:YES];
            return;
        }
        if ([result[@"cod"] isEqual:@"404"]) {
            [self.activityIndicator stopAnimating];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Not found city" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
            [self.navigationController popViewControllerAnimated:YES];
            return;
        }
        [self.activityIndicator stopAnimating];
        
        NSString *cityName = result[@"name"];
        NSNumber *ID = result[@"id"];
        NSNumber *lon = result[@"coord"][@"lon"];
        NSNumber *lan = result[@"coord"][@"lan"];
        
        if (self.switchResult) {
            NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([City class])];
            [request setPredicate:[NSPredicate predicateWithFormat:@"name == %@", cityName]];
            NSArray *cities = [self.managedObjectContext executeFetchRequest:request error:nil];
            if (!cities.count) {
                [City cityWithName:cityName id:ID lon:lon lan:lan];
                [self.myDelegate reload];
                [self.managedObjectContext save:nil];
            }
        }
        
        NSArray *weatherArray = result[@"weather"];
        NSDictionary *weatherDictionary = weatherArray[0];
        NSString *mainString = weatherDictionary[@"main"];
        
        Prediction *prediction = [Prediction new];
        prediction.main = weatherDictionary[@"main"];
        prediction.weatherDescription = weatherDictionary[@"description"];
        prediction.temp = result[@"main"][@"temp"];
        prediction.windSpeed = result[@"wind"][@"speed"];
        prediction.cityName = cityName;
        prediction.date = result[@"dt"];
        self.prediction = prediction;
        
        
    
        [self changeImage:mainString];
        [self.imageView setAlpha:0.4f];
        [self changeLabelTextColor:[UIColor blackColor]];
        [self addInformationOnView];
        [self hiddenAllTextLabel:NO];
    }];
}

#pragma mark Methods

- (void)changeLabelTextColor:(UIColor *)color
{
    //почитай, что такое IBOutletCollection. С их помощью избавишься от дублирования кода
    self.cityNameLabel.textColor = color;
    self.dateLabel.textColor = color;
    self.descriptionLabel.textColor = color;
    self.tempLabel.textColor = color;
    self.windSpeedLabel.textColor = color;
    self.temperatureLabel.textColor = color;
    self.windLabel.textColor = color;
}

- (void)hiddenAllTextLabel:(BOOL)hidden
{
    //почитай, что такое IBOutletCollection. С их помощью избавишься от дублирования кода
    self.cityNameLabel.hidden = hidden;
    self.dateLabel.hidden = hidden;
    self.descriptionLabel.hidden = hidden;
    self.tempLabel.hidden = hidden;
    self.windSpeedLabel.hidden = hidden;
    self.temperatureLabel.hidden = hidden;
    self.windLabel.hidden = hidden;
    self.predictionForSevenDaysButton.hidden = hidden;
}

- (void)changeImage:(NSString *)mainString
{
    self.imageView.image = [UIImage imageNamed:mainString];
    self.imageView.contentMode = UIViewContentModeScaleAspectFill;
}

- (void)addInformationOnView
{
    self.cityNameLabel.text = self.prediction.cityName;
    NSDateFormatter *format = [NSDateFormatter new];
    format.dateFormat = @"dd-MM-yyyy";
    self.dateLabel.text = [format stringFromDate:self.prediction.date];
    self.descriptionLabel.text = self.prediction.weatherDescription;
    self.tempLabel.text = [NSString stringWithFormat:@"%ld\u00B0C", (long)[self.prediction.temp integerValue]];
    self.windSpeedLabel.text = [NSString stringWithFormat:@"%ld m/s", (long)[self.prediction.windSpeed integerValue]];
}

#pragma mark Actions

- (IBAction)predictionForSevenDaysButton:(id)sender
{
    [self performSegueWithIdentifier:@"PredictionForSevenDays" sender:self];
}

#pragma mark Seque

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"PredictionForSevenDays"]) {
        PredictionForSevenDaysViewController *predictionForSevenDayViewController = (PredictionForSevenDaysViewController *)segue.destinationViewController;
        predictionForSevenDayViewController.cityName = self.prediction.cityName;
        predictionForSevenDayViewController.imageKey = self.prediction.main;
    }
}

@end
