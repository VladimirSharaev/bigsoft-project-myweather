//
//  PredictionForOneDayViewController.h
//  Your Weather
//
//  Created by Vladimir on 15.05.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol UpdateViewControllerDelegate <NSObject>

- (void)reload;

@end

@interface PredictionForOneDayViewController : UIViewController

@property (weak, nonatomic) id <UpdateViewControllerDelegate> myDelegate;
@property (nonatomic) NSString *cityName;
@property (nonatomic) BOOL switchResult;

@end
