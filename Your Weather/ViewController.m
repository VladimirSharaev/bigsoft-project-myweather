//
//  ViewController.m
//  Your Weather
//
//  Created by Vladimir on 15.05.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import "ViewController.h"
#import "Constants.h"
#import "CitiesCell.h"
#import "City+Creation.h"
#import "PredictionForOneDayViewController.h"
#import "AppDelegate.h"
#import <CoreData/CoreData.h>

@interface ViewController () <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UpdateViewControllerDelegate>

@property (nonatomic) NSArray *cities;
@property (nonatomic) NSString *currentCity;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (weak, nonatomic) IBOutlet UITextField *cityTextField;
@property (weak, nonatomic) IBOutlet UITableView *citiesTableView;
@property (weak, nonatomic) IBOutlet UISwitch *favoriteSwitch;


- (IBAction)goButton:(id)sender;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.cityTextField.delegate = self;
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    self.managedObjectContext = appDelegate.managedObjectContext;
    
    [self reload];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewDidDisappear:animated];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
}

#pragma mark Action

- (IBAction)goButton:(id)sender
{
    if (![self.cityTextField.text isEqual:@""]) {
        NSString *currentCityString = [self.cityTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        self.currentCity = currentCityString;
        [self performSegueWithIdentifier:@"PredictionForOneDay" sender:self];
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Enter correct city name" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
}

#pragma mark UIResponder

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [[event allTouches] anyObject];
    
    if (![[touch view] isKindOfClass:[UITextField class]]) {
        [self.view endEditing:YES];
    }
    [super touchesBegan:touches withEvent:event];
}

#pragma mark Text Field Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.cityTextField resignFirstResponder];
    return YES;
}

#pragma mark Table View Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.citiesTableView deselectRowAtIndexPath:indexPath animated:YES];
    self.favoriteSwitch.on = NO;
    City *city = self.cities[indexPath.row];
    self.currentCity = city.name;
    [self performSegueWithIdentifier:@"PredictionForOneDay" sender:self];
}

- (void)deselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.citiesTableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44.f;
}

#pragma mark Table View Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.cities.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifierUserCell = @"CitiesCell";
    CitiesCell *cell = [tableView dequeueReusableCellWithIdentifier:identifierUserCell];
    City *city = (City *)self.cities[indexPath.row];
    cell.nameCityLabel.text = city.name;
    return cell;

}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.managedObjectContext deleteObject:self.cities[indexPath.row]];
        [self reload];
        
        [self.managedObjectContext save:nil];
    }
}


#pragma mark Update View Controller Delegate

- (void)reload
{
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([City class])];
    NSSortDescriptor *nameSort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    request.sortDescriptors = @[nameSort];
    NSArray *cities = [self.managedObjectContext executeFetchRequest:request error:nil];
    self.cities = cities;
    [self.citiesTableView reloadData];
}

#pragma mark Seque

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"PredictionForOneDay"]) {
        PredictionForOneDayViewController *predictionForOneDayViewController = (PredictionForOneDayViewController *)segue.destinationViewController;
        predictionForOneDayViewController.myDelegate = self;
        predictionForOneDayViewController.switchResult = self.favoriteSwitch.on;
        predictionForOneDayViewController.cityName = self.currentCity;
    }
}


@end
