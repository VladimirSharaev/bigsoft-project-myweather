//
//  Prediction.h
//  Your Weather
//
//  Created by Vladimir on 15.05.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Prediction : NSObject

@property (nonatomic) NSString *cityName;
@property (nonatomic) NSDate *date;
@property (nonatomic) NSString *main;
@property (nonatomic) NSString *weatherDescription;
@property (nonatomic) NSNumber *temp;
@property (nonatomic) NSNumber *windSpeed;

@end
