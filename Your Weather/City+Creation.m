//
//  City+Creation.m
//  Your Weather
//
//  Created by Vladimir on 15.05.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import "City+Creation.h"
#import "AppDelegate.h"

@implementation City (Creation)

+ (City *)cityWithName:(NSString *)name id:(NSNumber *)ID lon:(NSNumber *)lon lan:(NSNumber *)lan
{
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = appDelegate.managedObjectContext;
    
    City *city = (City *)[NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass(self) inManagedObjectContext:context];
    if (city) {
        city.name = name;
        city.id = ID;
        city.lon = lon;
        city.lan = lan;
    }
    return city;
}

@end
