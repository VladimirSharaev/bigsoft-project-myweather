//
//  CitiesCell.m
//  Your Weather
//
//  Created by Vladimir on 15.05.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import "CitiesCell.h"

@implementation CitiesCell

- (UIEdgeInsets)layoutMargins {
    [super layoutMargins];
    
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

@end
